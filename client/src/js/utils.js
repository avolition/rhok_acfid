exports.headerData = [
  { linkName: 'Load', linkSlug: '/load' },
  { linkName: 'Config', linkSlug: '/config' },
  { linkName: 'Preview', linkSlug: '/preview' },
  { linkName: 'Download', linkSlug: '/download' }
];

exports.colsToNum = {
  A: { char: 'A', num: 1, used: 0 },
  B: { char: 'B', num: 2, used: 0 },
  C: { char: 'C', num: 3, used: 0 },
  D: { char: 'D', num: 4, used: 0 },
  E: { char: 'E', num: 5, used: 0 },
  F: { char: 'F', num: 6, used: 0 },
  G: { char: 'G', num: 7, used: 0 },
  H: { char: 'H', num: 8, used: 0 },
  I: { char: 'I', num: 9, used: 0 },
  J: { char: 'J', num: 10, used: 0 },
  K: { char: 'K', num: 11, used: 0 },
  L: { char: 'L', num: 12, used: 0 },
  M: { char: 'M', num: 13, used: 0 },
  N: { char: 'N', num: 14, used: 0 },
  O: { char: 'O', num: 15, used: 0 },
  P: { char: 'P', num: 16, used: 0 },
  Q: { char: 'Q', num: 17, used: 0 },
  R: { char: 'R', num: 18, used: 0 },
  S: { char: 'S', num: 19, used: 0 },
  T: { char: 'T', num: 20, used: 0 },
  U: { char: 'U', num: 21, used: 0 },
  V: { char: 'V', num: 22, used: 0 },
  W: { char: 'W', num: 23, used: 0 },
  X: { char: 'X', num: 24, used: 0 },
  Y: { char: 'Y', num: 25, used: 0 },
  Z: { char: 'Z', num: 26, used: 0 },
  AA: { char: 'AA', num: 27, used: 0 },
  AB: { char: 'AB', num: 28, used: 0 },
  AC: { char: 'AC', num: 29, used: 0 },
  AD: { char: 'AD', num: 30, used: 0 },
  AE: { char: 'AE', num: 31, used: 0 },
  AF: { char: 'AF', num: 32, used: 0 },
  AG: { char: 'AG', num: 33, used: 0 },
  AH: { char: 'AH', num: 34, used: 0 },
  AI: { char: 'AI', num: 35, used: 0 },
  AJ: { char: 'AJ', num: 36, used: 0 },
  AK: { char: 'AK', num: 37, used: 0 },
  AL: { char: 'AL', num: 38, used: 0 },
  AM: { char: 'AM', num: 39, used: 0 },
  AN: { char: 'AN', num: 40, used: 0 },
  AO: { char: 'AO', num: 41, used: 0 },
  AP: { char: 'AP', num: 42, used: 0 },
  AQ: { char: 'AQ', num: 43, used: 0 },
  AR: { char: 'AR', num: 44, used: 0 },
  AS: { char: 'AS', num: 45, used: 0 },
  AT: { char: 'AT', num: 46, used: 0 },
  AU: { char: 'AU', num: 47, used: 0 },
  AV: { char: 'AV', num: 48, used: 0 },
  AW: { char: 'AW', num: 49, used: 0 },
  AX: { char: 'AX', num: 50, used: 0 },
  AY: { char: 'AY', num: 51, used: 0 },
  AZ: { char: 'AZ', num: 52, used: 0 },
  BA: { char: 'BA', num: 53, used: 0 },
  BB: { char: 'BB', num: 54, used: 0 },
  BC: { char: 'BC', num: 55, used: 0 },
  BD: { char: 'BD', num: 56, used: 0 },
  BE: { char: 'BE', num: 57, used: 0 },
  BF: { char: 'BF', num: 58, used: 0 },
  BG: { char: 'BG', num: 59, used: 0 },
  BH: { char: 'BH', num: 60, used: 0 },
  BI: { char: 'BI', num: 61, used: 0 },
  BJ: { char: 'BJ', num: 62, used: 0 },
  BK: { char: 'BK', num: 63, used: 0 },
  BL: { char: 'BL', num: 64, used: 0 },
  BM: { char: 'BM', num: 65, used: 0 },
  BN: { char: 'BN', num: 66, used: 0 },
  BO: { char: 'BO', num: 67, used: 0 },
  BP: { char: 'BP', num: 68, used: 0 },
  BQ: { char: 'BQ', num: 69, used: 0 },
  BR: { char: 'BR', num: 70, used: 0 },
  BS: { char: 'BS', num: 71, used: 0 },
  BT: { char: 'BT', num: 72, used: 0 },
  BU: { char: 'BU', num: 73, used: 0 },
  BV: { char: 'BV', num: 74, used: 0 },
  BW: { char: 'BW', num: 75, used: 0 },
  BX: { char: 'BX', num: 76, used: 0 },
  BY: { char: 'BY', num: 77, used: 0 },
  BZ: { char: 'BZ', num: 78, used: 0 },
  CA: { char: 'CA', num: 79, used: 0 },
  CB: { char: 'CB', num: 80, used: 0 },
  CC: { char: 'CC', num: 81, used: 0 },
  CD: { char: 'CD', num: 82, used: 0 },
  CE: { char: 'CE', num: 83, used: 0 },
  CF: { char: 'CF', num: 84, used: 0 },
  CG: { char: 'CG', num: 85, used: 0 },
  CH: { char: 'CH', num: 86, used: 0 },
  CI: { char: 'CI', num: 87, used: 0 },
  CJ: { char: 'CJ', num: 88, used: 0 },
  CK: { char: 'CK', num: 89, used: 0 },
  CL: { char: 'CL', num: 90, used: 0 },
  CM: { char: 'CM', num: 91, used: 0 },
  CN: { char: 'CN', num: 92, used: 0 },
  CO: { char: 'CO', num: 93, used: 0 },
  CP: { char: 'CP', num: 94, used: 0 },
  CQ: { char: 'CQ', num: 95, used: 0 },
  CR: { char: 'CR', num: 96, used: 0 },
  CS: { char: 'CS', num: 97, used: 0 },
  CT: { char: 'CT', num: 98, used: 0 },
  CU: { char: 'CU', num: 99, used: 0 },
  CV: { char: 'CV', num: 100, used: 0 },
  CW: { char: 'CW', num: 101, used: 0 },
  CX: { char: 'CX', num: 102, used: 0 },
  CY: { char: 'CY', num: 103, used: 0 },
  CZ: { char: 'CZ', num: 104, used: 0 }
};

exports.numToCols = {
  1: 'A',
  2: 'B',
  3: 'C',
  4: 'D',
  5: 'E',
  6: 'F',
  7: 'G',
  8: 'H',
  9: 'I',
  10: 'J',
  11: 'K',
  12: 'L',
  13: 'M',
  14: 'N',
  15: 'O',
  16: 'P',
  17: 'Q',
  18: 'R',
  19: 'S',
  20: 'T',
  21: 'U',
  22: 'V',
  23: 'W',
  24: 'X',
  25: 'Y',
  26: 'Z'
};

exports.CONSTANTS = {
  inputHeaderRowNumDefault: 11,
  inputFirstDataRowNumDefault: 13,
  inputFirstColLetterDefault: 'D',
  inputLastColLetterDefault: 'O'
};

exports.singleCellToColDefault = [
  {
    inSheet: 'Read File-Survey Instructions',
    inCol: 'D',
    inRow: 25,
    outSheet: 'Combined org data',
    outCol: 'C',
    outColHead: 'Organisation'
  }
  // ,{
  //   inSheet: 'Stuff',
  //   inCol: 'D',
  //   inRow: 25,
  //   outSheet: 'Stuff',
  //   outCol: 'C',
  //   outColHead: 'Organisation'
  // },
  // {
  //   inSheet: 'Read File-Survey Instructions',
  //   inCol: 'D',
  //   inRow: 25,
  //   outSheet: 'Combined org data',
  //   outCol: 'C',
  //   outColHead: 'Organisation'
  // }
];

exports.orgSheetHeadings = [
  'Year of report',
  'Year data come from',
  'ID Number',
  'Category',
  'Size',
  'year end',
  'registered w oagds',
  'DGR Status',
  'if no third party tax ded',
  'ancp (yes/no)',
  'ancp member category',
  'faith based (y/n)',
  'if yes what faith',
  'when founded',
  'when start aid work',
  'full time employees',
  'part time employees',
  'total employees',
  'female full time',
  'female part time',
  'female employees',
  'male employees',
  'senior managers',
  'women senior managers',
  'volunteers in Australia',
  'volunteers overseas',
  'Head of agency gender',
  'number people on board',
  'number women on board',
  'number men on board',
  'gender of board head',
  'board gender quota (y/n)',
  'board quota %',
  'individual financial donors',
  'corporate donors'
];

exports.defaultConfig = [
  'Year of report',
  'Year data come from',
  'ID Number',
  'Category',
  'Size',
  'year end',
  'registered w oagds',
  'DGR Status',
  'if no third party tax ded',
  'ancp (yes/no)',
  'ancp member category',
  'faith based (y/n)',
  'if yes what faith',
  'when founded',
  'when start aid work',
  'full time employees',
  'part time employees',
  'total employees',
  'female full time',
  'female part time',
  'female employees',
  'male employees',
  'senior managers',
  'women senior managers',
  'volunteers in Australia',
  'volunteers overseas',
  'Head of agency gender',
  'number people on board',
  'number women on board',
  'number men on board',
  'gender of board head',
  'board gender quota (y/n)',
  'board quota %',
  'individual financial donors',
  'corporate donors'
];
