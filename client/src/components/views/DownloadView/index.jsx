import React from 'react';
import ReadFileForm from './ReadFileForm';
// import ValidationForm from './ValidationForm';
import AggregateForm from './AggregateForm';
import FileCards from './FileCards';

const LoadView = props => {
  console.log('aggregator props: ', props, Object.keys(props.files).length);
  return [
    <div className="column-1">
      <ReadFileForm
        files={props.files}
        setAppState={props.setAppState}
        setFormatFileToAppState={props.setFormatFileToAppState}
        loadedToggle={props.loadedToggle}
      />
      <AggregateForm
        aggregatePreview={props.aggregatePreview}
        formatFile={props.formatFile}
        loadNum={props.loadNum}
      />
    </div>,
    <div className="column-2">
      <button
        className="waves-effect waves-light btn"
        onClick={props.resetState}
        disabled={!props.loadNum}
      >
        Reset
      </button>
      <FileCards files={props.files} />
    </div>
  ];
};

export default LoadView;
