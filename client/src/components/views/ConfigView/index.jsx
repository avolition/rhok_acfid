import React from 'react';
import ConfigForm from './ConfigForm';
import { Link } from 'react-router-dom';

const ConfigView = props => (
  <form className="config-form">
    <h5 className="h5-header">Single Cell to Column (cell C12 => column A)</h5>
    {props.config.singleCellToCol.map((item, i) => (
      <ConfigForm {...item} key={item.id} i={i} setAppConfig={props.setAppConfig} />
    ))}
    <Link
      to="/preview"
      id="config-button"
      className="btn-floating btn-large waves-effect waves-light teal"
      title="view table"
    >
      <i className="material-icons">view_comfy</i>
    </Link>
  </form>
);

export default ConfigView;
