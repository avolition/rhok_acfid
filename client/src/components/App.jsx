import React, { Component } from 'react';
import NavBar from './NavBar';
import ConfigView from './views/ConfigView';
import LoadView from './views/LoadView';
import DownloadView from './views/DownloadView';
// import AggregatorView from './AggregatorView';
import Loader from 'react-loader';

import { Redirect, Route } from 'react-router-dom';

import XLSX from 'xlsx';
import shortid from 'shortid';
import * as dat from '../js/utils';

class App extends Component {
  state = {
    loaded: true,
    loadedFiles: {},
    config: {
      singleCellToCol: []
    },
    wb: XLSX.utils.book_new(),
    wbState: {
      sheetNamesList: { 0: { name: 'Errors' } },
      sheetNames: ['Errors'],
      sheets: { 0: { Errors: { name: 'Errors', json: {} } } }
    },
    sheetNames: {},
    sheets: {},
    outputFile: undefined,
    inputConfig: {
      inputHeaderRowNum: dat.CONSTANTS.inputHeaderRowNumDefault,
      inputFirstDataRowNum: dat.CONSTANTS.inputFirstDataRowNumDefault,
      inputFirstColLetter: dat.CONSTANTS.inputFirstColLetterDefault,
      inputLastColLetter: dat.CONSTANTS.inputLastColLetterDefault
    }
  };
  componentDidMount = () => {
    // load default processing config to state
    const singleCellToCol = dat.singleCellToColDefault.reduce((acc, curr) => {
      curr.id = shortid.generate();
      acc.push(curr);
      return acc;
    }, []); // add short id to each config entry
    const { config } = this.state;
    config.singleCellToCol = singleCellToCol;
    this.setState({ config });
    // this.downloadFile();
  };
  // fn to toggle loader gif overlay
  loadedToggle = bool => this.setState({ loaded: bool });
  // fn to set app state by state[key] = data
  setAppState = (key, data) => this.setState({ [key]: data });
  // fn to set config view form | state[config]
  setAppConfig = e => {
    const { dataset, name, id, value } = e.target;
    const { config } = this.state;
    config[dataset.loc].forEach(item => (id === item.id ? (item[name] = value) : item));
    this.setState({ config });
  };
  aggregatePreview = outputConfig => {
    const { wb, wbState, loadedFiles } = this.state;
    // let errors = { Errors: [] };
    const { singleCellToCol } = this.state.config;
    // create list of sheets
    let index = 1;
    singleCellToCol.forEach(dat => {
      if (!wbState.sheetNames.includes(dat.outSheet)) {
        wbState.sheetNames.push(dat.outSheet);
        wbState.sheets[index] = { name: dat.outSheet, json: {} };
        index++;
      }
    });

    console.log('wbState: ', wbState);

    // outputFile.SheetNames = sheetConfig.SheetNames;
    // outputFile.Sheets = sheetConfig.Sheets;
    // outputFile.Sheets
    // console.log(newFile);
    // const outputData = [];
    // Object.values(this.state.files).forEach((dat, i) => {
    //   const input = dat.workbook;
    //   const row = i + 2; // start from row 2
    //   const year = new Date(Date.now()).getFullYear();
    //   console.log(dat);
    // process singleCellToCol data

    // singleCellToCol.forEach(item => {
    //   const { inSheet, inCol, inRow, outSheet, outCol, outColHead } = item;
    //   outputFile.Sheets[outSheet][`${outCol}1`] = outColHead;
    //   if (!input[inSheet] || !input[inSheet][`${inCol}${inRow}`]) {
    //     let errorMsg = `ERROR in ${dat.file.name} | `;
    //     errorMsg += input[inSheet]
    //       ? `sheet "${inSheet}" not found`
    //       : `${inCol}${inRow} not found in "${inSheet}"`;
    //     errorMessages.push(errorMsg);
    //   } else {
    //     outputFile.Sheets[outSheet][`${outCol}${row}`] = XLSX.utils.format_cell(
    //       input[inSheet][`${inCol}${inRow}`]
    //     );
    //   }
    // });

    //   //  const outputFile.SheetNames[config.singleCellToCol] = dat.workbook[]

    //   //   // const surveyA = input.SheetNames['Survey Part A'];
    //   //   // const surveyB = input.SheetNames['Survey Part B'];
    //   //   // console.log(organisation, surveyA, surveyB);

    //   //   // INSERT DATA - "Combined org data"
    //   //   const sheet1 = 'Combined org data';
    //   //   console.log(file.Sheets[sheet1], `A${row}`);
    //   //   file.Sheets[sheet1][`A${row}`].v = config.reportYear || year;
    //   //   file.Sheets[sheet1][`B${row}`].v = config.dataYear || year - 1;
    //   //   // const organisation
    //   //   file.Sheets[sheet1][`C${row}`].v = input.Sheets['Read First-Survey Instructions'].D25.v;
    //   //   file.Sheets[sheet1][`D${row}`].v = 'manually Added';
    //   //   file.Sheets[sheet1][`E${row}`].v = 'manually Added';
    //   //   file.Sheets[sheet1][`F${row}`].v = 'manually Added';
    //   //   const sheet2 = 'Survey Part A';
    //   //   // // OAGDS
    //   //   console.log('input.Sheets[sheet2].G12.v: ', input.Sheets[sheet2].G12.v);
    //   //   file.Sheets[sheet1][`G${row}`].v = input.Sheets[sheet2].G12.v || null;
    //   //   // // DGR
    //   //   file.Sheets[sheet1][`H${row}`].v = input.Sheets[sheet2].G13.v || null;
    //   //   // // TaxDed
    //   //   file.Sheets[sheet1][`I${row}`].v = input.Sheets[sheet2].G14.v || null;
    //   //   // // ANCP
    //   //   file.Sheets[sheet1][`J${row}`].v = input.Sheets[sheet2].G15.v || null;
    //   //   // // ANCPCat
    //   //   file.Sheets[sheet1][`K${row}`].v = input.Sheets[sheet2].G16.v || null;
    //   //   // // Faith
    //   //   file.Sheets[sheet1][`L${row}`].v = input.Sheets[sheet2].G17.v || null;
    //   //   // // WhichFaith
    //   //   file.Sheets[sheet1][`M${row}`].v = input.Sheets[sheet2].G18.v || null;
    //   //   // // WhenFounded
    //   //   file.Sheets[sheet1][`N${row}`].v = input.Sheets[sheet2].G19.v || null;
    //   //   // // WhenStartAidWork
    //   //   file.Sheets[sheet1][`O${row}`].v = input.Sheets[sheet2].G20.v || null;
    //   //   // // FullTime
    //   //   file.Sheets[sheet1][`P${row}`].v = input.Sheets[sheet2].G25.v || null;
    //   //   // // PartTime
    //   //   file.Sheets[sheet1][`Q${row}`].v = input.Sheets[sheet2].G26.v || null;
    //   //   // // TotalEmp
    //   //   file.Sheets[sheet1][`R${row}`].v = input.Sheets[sheet2].G27.v || null;
    //   //   // // FemaleEmpFullTime
    //   //   const FemaleEmpFullTime = input.Sheets[sheet2].G28.v || null;
    //   //   file.Sheets[sheet1][`S${row}`].v = FemaleEmpFullTime;
    //   //   // // FemaleEmpPartTime
    //   //   const FemaleEmpPartTime = input.Sheets[sheet2].G29.v || null;
    //   //   file.Sheets[sheet1][`T${row}`].v = FemaleEmpPartTime;
    //   //   // // FemaleEmp
    //   //   const FemaleEmp = FemaleEmpFullTime + FemaleEmpPartTime || null;
    //   //   file.Sheets[sheet1][`U${row}`].v = FemaleEmp;
    //   //   // // MaleEmp
    //   //   file.Sheets[sheet1][`V${row}`].v = 'newQ';
    //   //   // // SeniorMgrs
    //   //   file.Sheets[sheet1][`W${row}`].v = input.Sheets[sheet2].G31.v || null;
    //   //   // // SeniorMgrsWomen
    //   //   file.Sheets[sheet1][`X${row}`].v = input.Sheets[sheet2].G32.v || null;
    //   //   // // VolsAus
    //   //   file.Sheets[sheet1][`Y${row}`].v = input.Sheets[sheet2].G33.v || null;
    //   //   // // VolsOS
    //   //   file.Sheets[sheet1][`Z${row}`].v = input.Sheets[sheet2].G35.v || null;
    //   //   // // HeadGender
    //   //   file.Sheets[sheet1][`AA${row}`].v = input.Sheets[sheet2].G36.v || null;
    //   //   // // BoardNum
    //   //   // // BoardWomen
    //   //   // file.Sheets[sheet2][].v = input.Sheets[sheet2].G37.v;
    //   //   // // BoardHeadGender
    //   //   // file.Sheets[sheet2][].v = input.Sheets[sheet2].G38.v;
    //   //   // // BoardQuota
    //   //   // file.Sheets[sheet2][].v = input.Sheets[sheet2].G39.v;
    //   //   // // BoardQuotaPercent
    //   //   // file.Sheets[sheet2][].v = input.Sheets[sheet2].G40.v;
    //   //   // // IndivDonors
    //   //   // file.Sheets[sheet2][].v = input.Sheets[sheet2].G42.v;
    //   //   // // CorporateDonors
    //   //   // file.Sheets[sheet2][].v = input.Sheets[sheet2].G43.v;

    //   //   // // INSERT DATA - "Survey Part D"
    //   //   // ReportingPeriod = "Survey Part D" E11
    //   //   // Donations = "Survey Part D" E15
    //   //   // NonMonetaryDonations = "Survey Part D" E16
    //   //   // Bequests = "Survey Part D" E17
    //   //   // GrantsDFAT = "Survey Part D" E18
    //   //   // GrantsOtherAus = "Survey Part D" E19
    //   //   // GrantsOtherOverseas = "Survey Part D" E20
    //   //   // InvestmentInc = "Survey Part D" E21
    //   //   // OtherInc = "Survey Part D" E22
    //   //   // RIPRAPP = "Survey Part D" E23
    //   //   // RevforDom = "Survey Part D" E24
    //   //   // OtherIncNotACFIDForm = "Survey Part D" E25
    //   //   // TotalRevenue = "Survey Part D" E26
    //   //   // IntFunds = "Survey Part D" E29
    //   //   // IntProgSup = "Survey Part D" E30
    //   //   // ComEdn = "Survey Part D" E31
    //   //   // PublicFundRaiseCost = "Survey Part D" E32
    //   //   // GovtMultiLatFundRaiseCoast = "Survey Part D" E33
    //   //   // AccyandAdmin = "Survey Part D" E34
    //   //   // NonMonExp = "Survey Part D" E35
    //   //   // TotalAidExp = "Survey Part D" E36
    //   //   // EIPRAPP = "Survey Part D" E37
    //   //   // DomProjExp = "Survey Part D" E38
    //   //   // OtherExpNotACFIDForm = "Survey Part D" E39
    //   //   // TotalExp = "Survey Part D" E40
    //   //   // RevVExp = "Survey Part D" E42

    //   //   // INSERT DATA - "Combined proj data"
    // });
    // if (errorMessages.length) {
    //   outputFile.Sheets.Errors = errorMessages.reduce((acc, errMsg, i) => {
    //     acc[`A${i + 4}`] = this.createCell(errMsg);
    //     return acc;
    //   }, {});
    //   outputFile.Sheets.Errors.A1 = this.createCell(`Total Errors = errorMessages.length`);
    //   outputFile.Sheets.Errors.A3 = this.createCell(`Errors:`);
    // } else {
    //   // remove Errors sheet from output
    //   outputFile.SheetNames.shift();
    //   delete outputFile.Sheets.Errors;
    // }

    // headings.forEach((heading, i) => {
    //   doc[`A${i + 1}`].v = heading;
    // });
    // doc.SheetNames
    // outputFile.SheetNames[0] = 'Sheet Name';
    // console.log('outputFile: ', outputFile);
    // doc.
    // Object.values(this.state.data).forEach(file => {
    //
    //
    //   'Combined org data'
    // })

    // this.downloadFile(outputFile, outputConfig);
  };
  downloadFile = (outputFile, fileConfig) => {
    fileConfig = {};
    // const fileName =
    //   fileConfig.fileName ||
    //   `CombinedData_${new Date(Date.now())
    //     .toLocaleString()
    //     .replace(', ', '_')
    //     .replace(/\/|:/g, '-')}`;
    // const bookType = fileConfig.fileFormat || 'xlsx';

    outputFile = XLSX.utils.book_new();
    outputFile.SheetNames.push('TestSheet');
    const TestSheet = {
      0: { A: 'Sheet', B: 'h', C: 'e', D: 'e', E: 't', F: 'J', G: 'S' },
      1: { A: 1, B: 2, C: 3, D: 4, E: 5, F: 6, G: 7, H: null, I: undefined, J: 9 },
      2: { A: 2, B: 3, C: 4, D: 5, E: 6, F: 7, G: 8 }
    };
    outputFile.Sheets.TestSheet = XLSX.utils.json_to_sheet(Object.values(TestSheet).map(el => el), {
      header: [],
      skipHeader: true
    });

    // // // write sheet to table
    // const test = document.createElement('html');
    // test.innerHTML = XLSX.utils.sheet_to_html(outputFile.Sheets.TestSheet);
    // document.querySelector('body').appendChild(test.querySelector('table'));
    // // //

    // console.log(test, table);
    // XLSX.writeFile(outputFile, `${fileName}.${bookType}`);
  };
  setInputConfig = config => {
    const inputConfig = {
      inputHeaderRowNum: config.inputHeaderRowNum || dat.CONSTANTS.inputHeaderRowNumDefault,
      inputFirstDataRowNum:
        config.inputFirstDataRowNum || dat.CONSTANTS.inputFirstDataRowNumDefault,
      inputFirstColLetter: config.inputFirstColLetter || dat.CONSTANTS.inputFirstColLetterDefault,
      inputLastColLetter: config.inputLastColLetter || dat.CONSTANTS.inputLastColLetterDefault
    };
    this.setState({ inputConfig });
  };

  // setFormatFileToAppState = formatFile => this.setState({ formatFile });

  render() {
    const { loaded, loadedFiles, config } = this.state;
    const loadNum = Object.keys(loadedFiles).length;
    return (
      <div className="app">
        <Loader loaded={loaded} />
        <NavBar brand="Excel Aggregator" headerData={dat.headerData} right />
        <Route exact path="/" render={() => <Redirect to="/load" />} />
        <div className="main">
          <Route
            exact
            path="/load"
            render={() => (
              <LoadView
                loaded={loaded}
                loadedToggle={this.loadedToggle}
                loadedFiles={loadedFiles}
                config={config}
                setAppConfig={this.setAppConfig}
                setAppState={this.setAppState}
                aggregatePreview={this.aggregatePreview} // TODO:  remove
              />
            )}
          />
          <Route
            exact
            path="/config"
            render={() => (
              <ConfigView
                loaded={loaded}
                loadedFiles={loadedFiles}
                config={config}
                setAppConfig={this.setAppConfig}
              />
            )}
          />
          <Route
            exact
            path="/preview"
            // render={
            //   () => {}
            // <LoadView
            //   files={files}
            //   loadNum={loadNum}
            //   setAppState={this.setAppState}
            //   setFormatFileToAppState={this.setFormatFileToAppState}
            //   aggregatePreview={this.aggregatePreview}
            //   loaded={loaded}
            //   loadedToggle={this.loadedToggle}
            // />
          />
          <Route
            exact
            path="/download"
            render={() => (
              <DownloadView
                loadedFiles={loadedFiles}
                loadNum={loadNum}
                setAppState={this.setAppState}
                setFormatFileToAppState={this.setFormatFileToAppState}
                aggregatePreview={this.aggregatePreview}
                loaded={loaded}
                loadedToggle={this.loadedToggle}
              />
            )}
          />
        </div>
      </div>
    );
  }
}

export default App;
