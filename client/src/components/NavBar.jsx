import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';

const NavBar = props => (
  <nav className="nav-wrapper">
    <div className="brand-logo left">{props.brand}</div>
    <ul className="right">
      {props.headerData.map((l, i) => (
        <li key={i}>
          <NavLink to={l.linkSlug} activeStyle={{ background: '#ea454b' }}>
            {l.linkName}
          </NavLink>
        </li>
      ))}
    </ul>
  </nav>
);

export default withRouter(NavBar);
