import React, { Component } from 'react';
import XLSX from 'xlsx';
import shortid from 'shortid';

class ReadFileForm extends Component {
  state = {
    inputFileList: [],
    inputFormatFile: []
  };
  // fn to read binary file input and write as xlsx object to App state
  readFilesToState = (e, file, i, format = undefined) => {
    const fileAsBinaryString = e.target.result;
    const workbook = XLSX.read(fileAsBinaryString, { type: 'binary' });
    const { files, setAppState, setFormatFileToAppState } = this.props;
    if (format) {
      console.log('workbook', workbook);
      return setFormatFileToAppState(workbook);
    }
    const newId = shortid.generate();
    files[newId] = {
      id: newId,
      file,
      workbook
    };
    console.log('files', files);
    setAppState(files);
    if (i >= this.state.inputFileList.length - 2) {
      this.setState({ inputFileList: [], inputFormatFile: [] });
      this.props.loadedToggle(true);
    }
  };
  // fn to bulk read input files and hand to readFilesToState()
  parseFiles = e => {
    e.preventDefault();
    this.props.loadedToggle(false);
    // const formatFile = e.target.userFormatFile.files[0];
    // const reader = new FileReader();
    // reader.onload = e => this.readFilesToState(e, formatFile, 0, 'format');
    // reader.readAsBinaryString(formatFile);
    const inputFileList = e.target.userFiles.files;
    Object.values(inputFileList).forEach((file, i) => {
      const reader = new FileReader();
      reader.onload = e => this.readFilesToState(e, file, i);
      reader.readAsBinaryString(file);
    });
    document.getElementById('read-file-form').reset();
  };
  setInputFileList = e => {
    const inputFileList = Object.values(e.target.files).map(i => i.name);
    this.setState({ inputFileList });
  };
  // setInputFormatFile = e => {
  //   const inputFormatFile = Object.values(e.target.files).map(i => i.name)[0];
  //   this.setState({ inputFormatFile });
  // };
  render() {
    const fileList = this.state.inputFileList;
    const fileDisplay =
      fileList.length > 1 ? `${fileList.length} files selected for loading` : fileList[0];
    return (
      <form
        id="read-file-form"
        method="post"
        encType="multipart/form-data"
        onSubmit={e => this.parseFiles(e)}
      >
        <div className="file-field input-field">
          <div className="btn">
            {/* Multi file input field */}
            <span>1) Select Files</span>
            <input
              type="file"
              name="userFiles"
              multiple
              required
              onChange={this.setInputFileList}
            />
          </div>
          <div className="file-path-wrapper">
            <input
              className={fileDisplay ? 'file-path validate files' : 'file-path validate'}
              type="text"
              placeholder={fileDisplay ? fileDisplay : 'Load one or more files to aggregate'}
            />
          </div>
        </div>
        <button className="waves-effect waves-light btn">
          2) Load Files<i className="material-icons right">file_upload</i>
        </button>
      </form>
    );
  }
}

export default ReadFileForm;
