import React, { Component } from 'react';

class AggregateForm extends Component {
  outputFileNameRef = React.createRef();
  outputFileFormatRef = React.createRef();
  reportYearRef = React.createRef();
  dataYearRef = React.createRef();

  triggerDownload = e => {
    e.preventDefault();
    const outputConfig = {
      fileName: this.outputFileNameRef.current.value,
      format: this.outputFileFormatRef.current.value,
      reportYear: this.reportYearRef.current.value,
      dataYear: this.dataYearRef.current.value
    };
    this.aggregateFormRef.reset();
    this.props.aggregatePreview(outputConfig);
  };
  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.formatFile !== this.props.formatFile) this.forceUpdate();
  };
  render() {
    const { loadNum } = this.props;
    return (
      <form
        className="output-config-form"
        method="post"
        onSubmit={e => this.triggerDownload(e)}
        ref={el => (this.aggregateFormRef = el)}
      >
        <input
          type="number"
          name="reportYear"
          ref={this.reportYearRef}
          placeholder="Enter Reporting Year"
        />
        <input type="number" name="dataYear" ref={this.dataYearRef} placeholder="Enter Data Year" />
        <input
          type="text"
          name="outputFileName"
          ref={this.outputFileNameRef}
          placeholder="Enter Output File Name"
        />
        <select
          className="browser-default"
          name="fileFormat"
          id="fileFormat"
          ref={this.outputFileFormatRef}
        >
          <option value="" default>
            select output file type
          </option>
          <option value="xlsx">xlsx</option>
        </select>
        <br />
        <button disabled={!loadNum} className="waves-effect waves-light btn">
          3) Aggregate & Download {loadNum === 1 ? '1 file' : `${loadNum} files`}
        </button>
      </form>
    );
  }
}

export default AggregateForm;
