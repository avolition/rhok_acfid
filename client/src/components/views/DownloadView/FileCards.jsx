import React from 'react';

const FileCards = props => (
  <div className="card-list">
    {Object.values(props.files)
      .sort((a, b) => (a.file.name < b.file.name ? -1 : 1))
      .map(dat => (
        <div className="card-panel blue-grey darken-1" key={dat.id}>
          {dat.file.name}
        </div>
      ))}
  </div>
);

export default FileCards;
