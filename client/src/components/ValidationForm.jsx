import React, { Component } from 'react';
// import XLSX from 'xlsx';
import { CONSTANTS } from '../js/utils';

class ValidationForm extends Component {
  inputHeaderRowNumRef = React.createRef();
  inputFirstDataRowNumRef = React.createRef();
  inputFirstColLetterRef = React.createRef();
  inputLastColLetterRef = React.createRef();

  validateFiles = e => {
    e.preventDefault();
    const inputConfig = {
      inputHeaderRowNum: this.inputHeaderRowNumRef.current.value,
      inputFirstDataRowNum: this.inputFirstDataRowNumRef.current.value,
      inputFirstColLetter: this.inputFirstColLetterRef.current.value,
      inputLastColLetter: this.inputLastColLetterRef.current.value
    };
    this.validateFormRef.reset();
  };
  render() {
    return (
      <form
        method="post"
        onSubmit={e => this.validateFiles(e)}
        ref={el => (this.validateFormRef = el)}
      >
        <input
          type="number"
          name="inputHeaderRowNum"
          placeholder={`Header Row Number? (Default: ${CONSTANTS.inputHeaderRowNumDefault})`}
          ref={this.inputHeaderRowNumRef}
        />
        <input
          type="number"
          name="inputFirstDataRowNum"
          placeholder={`First Data Row? (Default: ${CONSTANTS.inputFirstDataRowNumDefault})`}
          ref={this.inputFirstDataRowNumRef}
        />
        <input
          type="text"
          name="inputFirstColLetter"
          placeholder={`First Column Letter? (Default: "${CONSTANTS.inputFirstColLetterDefault}")`}
          ref={this.inputFirstColLetterRef}
        />
        <input
          type="text"
          name="inputLastColLetter"
          placeholder={`Last Column Letter? (Default: "${CONSTANTS.inputLastColLetterDefault}")`}
          ref={this.inputLastColLetterRef}
        />
        <button>Validate Workbook(s) Configuration</button>
      </form>
    );
  }
}

// <input type="text" name="headerRow" placeholder="Enter row number that contains headings" />

export default ValidationForm;
