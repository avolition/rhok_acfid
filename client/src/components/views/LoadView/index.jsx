import React from 'react';
import ReadFileForm from './ReadFileForm';
import FileCards from './FileCards';

const LoadView = props => {
  const { loadedFiles, setAppState, loadedToggle, aggregatePreview } = props;
  const loadedFilesNum = Object.keys(loadedFiles).length;
  console.log('LoadView props: ', props, Object.keys(loadedFiles).length);
  return (
    <React.Fragment>
      <div className="column-1">
        <ReadFileForm
          loadedFiles={loadedFiles}
          setAppState={setAppState}
          loadedToggle={loadedToggle}
          aggregatePreview={aggregatePreview}
        />
      </div>
      <div className="column-2">
        <button
          className="waves-effect waves-light btn"
          onClick={() => setAppState('loadedFiles', {})}
          disabled={!loadedFilesNum}
        >
          {`Reset ${loadedFilesNum ? loadedFilesNum : ''} File${loadedFilesNum === 1 ? '' : 's'}`}
        </button>
        <FileCards displayFiles={loadedFiles} />
      </div>
    </React.Fragment>
  );
};

export default LoadView;
