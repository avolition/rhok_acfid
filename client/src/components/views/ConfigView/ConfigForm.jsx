import React from 'react';
// import React, { Component } from 'react';

const ConfigInputForm = props => (
  <div className="row">
    <div className="inner-row">
      <div className="input-row">
        <div className="input-field col s8">
          {props.i === 0 ? <label htmlFor="inSheet">Input Sheet Name</label> : null}
          <input
            data-loc="singleCellToCol"
            name="inSheet"
            id={props.id}
            type="text"
            className="validate"
            value={props.inSheet}
            placeholder="Sheet Name"
            onChange={props.setAppConfig}
          />
        </div>
        <div className="input-field col s2">
          {props.i === 0 ? <label htmlFor="inCol">Column</label> : null}
          <input
            data-loc="singleCellToCol"
            name="inCol"
            id={props.id}
            type="text"
            className="validate center"
            placeholder="A"
            value={props.inCol}
            onChange={props.setAppConfig}
          />
        </div>
        <div className="input-field col s2">
          {props.i === 0 ? <label htmlFor="inRow">Row</label> : null}
          <input
            data-loc="singleCellToCol"
            name="inRow"
            id={props.id}
            type="number"
            min="1"
            className="validate center"
            placeholder="1"
            value={props.inRow}
            onChange={props.setAppConfig}
          />
        </div>
      </div>
    </div>
    <i className="material-icons">chevron_right</i>
    <div className="inner-row">
      <div className="input-row">
        <div className="input-field col s6">
          {props.i === 0 ? <label htmlFor="outSheet">Output Sheet Name</label> : null}
          <input
            data-loc="singleCellToCol"
            name="outSheet"
            id={props.id}
            type="text"
            className="validate"
            placeholder="Combined org data"
            value={props.outSheet}
            onChange={props.setAppConfig}
          />
        </div>
        <div className="input-field col s2">
          {props.i === 0 ? <label htmlFor="outCol">Column</label> : null}
          <input
            data-loc="singleCellToCol"
            name="outCol"
            id={props.id}
            type="text"
            className="validate center"
            placeholder="C"
            value={props.outCol}
            onChange={props.setAppConfig}
          />
        </div>
        <div className="input-field col s4">
          {props.i === 0 ? <label htmlFor="outColHead">Column Heading</label> : null}
          <input
            data-loc="singleCellToCol"
            name="outColHead"
            id={props.id}
            type="text"
            className="validate"
            placeholder="Organisation"
            value={props.outColHead}
            onChange={props.setAppConfig}
          />
        </div>

        <i className="material-icons">add_circle_outline</i>
        {props.i === 0 ? null : <i className="material-icons">remove_circle_outline</i>}
      </div>
    </div>
  </div>
  // props.arr.map((entry, i) => (
  //   <div key={i}>
  //     {i === 0 ? <h5>{props.statename}</h5> : null}
  //     <Form.Group widths="equal">
  //       <Form.Input
  //         type="text"
  //         placeholder={`${props.statename} Name #${i + 1}`}
  //         statename={props.statename.toLowerCase()}
  //         name="name"
  //         value={entry.name}
  //         onChange={(e, fields) => props.handleObjChange(e, i, fields)}
  //       />
  //       <Form.Input
  //         type="email"
  //         placeholder={`${props.statename} Email #${i + 1}`}
  //         statename={props.statename.toLowerCase()}
  //         name="email"
  //         value={entry.email}
  //         onChange={(e, fields) => props.handleObjChange(e, i, fields)}
  //         // required if first 'To' row
  //         required={props.statename === 'To' && i + 1 === 1 ? true : false}
  //       />
  //       {/* add(+) button htmlFor first row, remove(-) button htmlFor all others */}
  //       {i > 0 ? (
  //         <Button
  //           size="mini"
  //           statename={props.statename.toLowerCase()}
  //           onClick={(e, fields) => props.handleObjRemove(e, i, fields)}
  //           title="remove"
  //         >
  //           -
  //         </Button>
  //       ) : (
  //         <Button
  //           size="mini"
  //           statename={props.statename.toLowerCase()}
  //           onClick={(e, fields) => props.handleObjAdd(e, fields)}
  //           title="add another"
  //         >
  //           +
  //         </Button>
  //       )}
  //     </Form.Group>
  //   </div>
);

export default ConfigInputForm;
