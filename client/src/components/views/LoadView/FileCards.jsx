import React from 'react';

const FileCards = props => {
  const { displayFiles, duplicates } = props;
  return (
    <div className="card-list">
      {displayFiles && displayFiles.length
        ? displayFiles.sort((a, b) => (a < b ? -1 : 1)).map(name => (
            <div
              className={`card-panel blue-grey darken-1 ${
                duplicates && duplicates.fileNames.includes(name) ? 'duplicate' : ''
              }`}
              key={name}
            >
              {name}
            </div>
          ))
        : Object.values(displayFiles)
            .sort((a, b) => (a.file.name < b.file.name ? -1 : 1))
            .map(dat => (
              <div
                className={`card-panel blue-grey darken-1 ${
                  duplicates && duplicates.fileNames.includes(dat.file.name) ? 'duplicate' : ''
                }`}
                key={dat.id}
              >
                {dat.file.name}
              </div>
            ))}
    </div>
  );
};

export default FileCards;
