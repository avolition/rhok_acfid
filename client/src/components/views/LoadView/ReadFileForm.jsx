import React, { Component } from 'react';
import FileCards from './FileCards';
import XLSX from 'xlsx';
import shortid from 'shortid';

class ReadFileForm extends Component {
  state = {
    inputFileList: []
  };
  // fn to read single binary file input and write as xlsx object to App state
  readFilesToState = (e, file, i) => {
    const fileAsBinaryString = e.target.result;
    const wb = XLSX.read(fileAsBinaryString, { type: 'binary' });
    const { loadedFiles, setAppState, aggregatePreview } = this.props;
    const newId = shortid.generate();
    loadedFiles[newId] = {
      id: newId,
      file,
      wb
    };
    console.log('loadedFiles', loadedFiles);
    setAppState('loadedFiles', loadedFiles); // set App.state.loadedFiles
    if (i >= this.state.inputFileList.length - 2) {
      this.setState({ inputFileList: [] });
      this.props.loadedToggle(true);
      aggregatePreview();
    }
  };
  // fn to bulk read input files and hand to readFilesToState()
  parseFiles = e => {
    e.preventDefault();
    this.props.loadedToggle(false);
    // const formatFile = e.target.userFormatFile.files[0];
    // const reader = new FileReader();
    // reader.onload = e => this.readFilesToState(e, formatFile, 0, 'format');
    // reader.readAsBinaryString(formatFile);
    const inputFileList = e.target.userFiles.files;
    Object.values(inputFileList).forEach((file, i) => {
      const reader = new FileReader();
      reader.onload = e => this.readFilesToState(e, file, i);
      reader.readAsBinaryString(file);
    });
    document.getElementById('read-file-form').reset();
  };
  // fn to load input file list to state to display number of selected files
  setInputFileList = e => {
    const inputFileList = Object.values(e.target.files).map(i => i.name);
    this.setState({ inputFileList });
  };
  render() {
    const loadedFileNames = Object.values(this.props.loadedFiles).map(file => file.file.name);
    const fileList = this.state.inputFileList;
    const duplicates = { bool: false, fileNames: [] };
    if (fileList.length) {
      fileList.forEach(name => {
        if (loadedFileNames.includes(name)) {
          duplicates.bool = true;
          duplicates.fileNames.push(name);
        }
      });
    }
    const fileDisplay =
      fileList.length > 1 ? `${fileList.length} files selected for loading` : fileList[0];
    // console.log(this.props, this.state, fileNames);
    return (
      <form
        id="read-file-form"
        method="post"
        encType="multipart/form-data"
        onSubmit={e => this.parseFiles(e)}
      >
        <div className="file-field input-field">
          <div className="btn">
            {/* Multi file input field */}
            <span>{`1) Select Files`}</span>
            <input
              type="file"
              name="userFiles"
              multiple
              required
              onChange={this.setInputFileList}
            />
          </div>
          <div className="file-path-wrapper">
            <input
              className={fileDisplay ? 'file-path validate files' : 'file-path validate'}
              type="text"
              placeholder={fileDisplay ? fileDisplay : 'Load one or more files to aggregate'}
            />
          </div>
        </div>
        {/* button to load 1+ files to state */}
        <button
          className="waves-effect waves-light btn"
          disabled={!fileList.length || duplicates.bool}
        >
          {duplicates.bool
            ? 'Duplicate files selected - Re-select!'
            : `2) Load ${fileList.length ? fileList.length : ''} File${
                fileList.length === 1 ? '' : 's'
              }`}
          <i className="material-icons right">file_upload</i>
        </button>
        <FileCards displayFiles={fileList} duplicates={duplicates} />
      </form>
    );
  }
}

export default ReadFileForm;
